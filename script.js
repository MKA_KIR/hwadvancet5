const xhr = new XMLHttpRequest();
xhr.open('GET', "https://swapi.dev/api/films/");
xhr.responseType = 'json';
xhr.send();
xhr.onload = function() {
    if (xhr.status >= 300) {
        console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`);
    }
    else {
        const {results} = xhr.response;
        const ListStr = results.map(({episode_id, title, opening_crawl, characters}) => {
            const movieCard = document.createElement("div");
            movieCard.className = 'poster';
            movieCard.innerHTML = `<div class="poster__info">
                                        <h2 class="poster__title">${episode_id} : ${title}</h2>
                                        <p class="poster__text">${opening_crawl}</p>
                                    </div>`;
            const button = document.createElement('a');
            button.className = "show-all-btn";
            button.textContent = 'Get all characters';
            movieCard.insertAdjacentElement('beforeend', button);
            button.addEventListener('click', function (e) {
                e.preventDefault();
                movieCard.insertAdjacentHTML("beforeend", `<div class="all-characters"><h4>Characters list:</h4><ul class="characters-list"></ul></div>`);                
                characters.forEach(url => {
                    const req = new XMLHttpRequest();
                    req.open('GET', url);
                    req.responseType = 'json';
                    req.send();
                    req.onload = function () {
                        if (xhr.status >= 300) {
                            console.log(`Ошибка ${xhr.status}: ${xhr.statusText}`);
                        }
                        else {
                            const {name} = req.response;
                            const character = document.createElement('li');
                            character.textContent = name;
                            const charactersList = movieCard.querySelector(".characters-list");
                            charactersList.append(character);
                        }
                    }
                })
            });
            return movieCard;
        });
        const movieList = document.getElementById('movie_list');
        movieList.append(...ListStr);
    }
};
xhr.onerror = function() {
    alert("Запрос не удался");
};
